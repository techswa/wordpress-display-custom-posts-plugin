<?php
/*
Plugin Name: SWA wp display custom posts
Version: 1.0.0
Plugin URI:
Author: B Burt
Author URI: http://www.smarterwebapps.com/
*/

//Declare custom query vars
include_once('includes/controllers/swa-get-queryvars.php');

//Declare custom shortcodes for each query var
include_once('includes/controllers/custom-query-var-shortcodes.php');

//Shortcode to display terms of service
include_once('includes/controllers/swa-terms-of-service-shortcode.php');

//Shortcode to display faqs
include_once('includes/controllers/swa-display-faqs-shortcode.php');

//Shortcode to diplay benefits
include_once('includes/controllers/swa-display-benefit-shortcode.php.php');

//Shortcode to display secrets
include_once('includes/controllers/swa-credit-secrets-consumer-shortcode.php');

//Shortcode to display callouts
include_once('includes/controllers/swa-display-callouts-shortcode.php');
