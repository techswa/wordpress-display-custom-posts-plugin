<?php

//Display callouts post type

add_shortcode( 'swa_callout_consumer', 'swa_callout_consumer_func' );

function swa_callout_consumer_func($args){

	global $post;

	$element = $args['element'];

 	$page_id = $args['page_id'];

	$args = array(
						'post_type' => 'callout',
						'page_id' => $page_id
						);
	$output = ''; // Clear buffer

	$defaults = array(
							'post_type'	=> 'callout',
							'page_id'		=>	'586' //Callout default page
							);
	$args = wp_parse_args( $args, $defaults );

	$swa_query= new WP_Query( $args );

	if ( $swa_query->have_posts() ) {

		while ( $swa_query->have_posts() ) {
			$swa_query->the_post();

			switch($element){
				case 'title':
						$output .= get_the_title();
					break;

				case 'excerpt':
						$output .= get_the_excerpt();
					break;

				case 'body':
						$output .= get_the_content();
						break;

				default:
							$output = 'Please verify requested element';
							//$output .= get_the_content();
				}
			}
    //$output .= '</div>';
	} else {
		$output .= 'Please verify callout page id.';
	}

	wp_reset_postdata();


	return $output;
}
