<?php

/**
 * Get the current users working file path
 *
 * @param     object $current_user
 * @return    array $path_info  ( path, url )
 * @author
 * @copyright
 */

 function swa_get_current_users_home_directory_info( $current_user ){

   // get the current user's id
   $current_user_id = $current_user->ID;

   $state = get_user_meta($current_user_id,  $key = USER_META_LICENSE_STATE, $single = true);

   $license_type = get_user_meta($current_user_id,  $key = USER_META_LICENSE_TYPE, $single = true);

   // Adjust license type to address RE_BROKER type
   //Only allow NMLS and RE
   if($license_type == 'RE_BROKER'){
      $license_type = 'RE';
   }

   $user_name =$current_user->user_login;

   // get the wp base upload path
   $current_path_info = wp_upload_dir();

   // '/home/public_html/yourdomainfolder/new/path/';
   $new_path = "{$current_path_info['path']}/{$state}/{$license_type}/{$user_name}/";
  // 'http://yourdomainhere.com/new/path/';
   $new_url = "{$current_path_info['url']}/{$state}/{$license_type}/{$user_name}/";

   $path_info = array(
                'path'  => $new_path,
                'url'   => $new_url
   );

   return $path_info;
 }
