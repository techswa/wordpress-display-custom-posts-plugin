<?php

//Display faqs post type

add_shortcode( 'swa_faq_consumer', 'swa_faq_consumer_func' );

function swa_faq_consumer_func($args){

	$element = $args['element'];

	$page_id = $args['page_id'];

	$args = array(
						'post_type' => 'faq',
						'page_id' => $page_id
						);
	$output = ''; // Clear buffer

	$defaults = array(
							'post_type'	=> 'faq',
							'page_id'		=>	'602' //Accordion default page
							);
	$args = wp_parse_args( $args, $defaults );

	$swa_query= new WP_Query( $args );
	if ( $swa_query->have_posts() ) {

		//Get the current record
		$swa_query->the_post();

		switch($element){
			case 'title':
					$output .= get_the_title();
				break;

			case 'excerpt':
					$output .= get_the_excerpt();
				break;

			case 'body':
					$output .= get_the_content();
					break;

			default:
						$output = 'Please verify requested element';
						//$output .= get_the_content();
			}
		//}

	} else {
		$output .= 'Default consumer faqs not found.';
	}

	wp_reset_postdata();


	return $output;
}
