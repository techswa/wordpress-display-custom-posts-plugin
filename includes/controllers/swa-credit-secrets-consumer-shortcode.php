<?php

//Display secrets post type short code

add_shortcode( 'swa_secrets_consumer', 'swa_secrets_consumer_func' );

// [swa_secrets_consumer page_id='582' element='body']
function swa_secrets_consumer_func($args){

	$element = $args['element'];

	$page_id = $args['page_id'];

	$args = array(
						'post_type' => 'secret',
						'page_id' => $page_id
						);
	$output = ''; // Clear buffer

	$defaults = array(
							'post_type'	=> 'secret',
							'page_id'		=>	'618' //Default page
							);
	$args = wp_parse_args( $args, $defaults );

	$swa_query= new WP_Query( $args );
	if ( $swa_query->have_posts() ) {

		//Get the current record
		$swa_query->the_post();

		switch($element){
			case 'title':
					$output .= get_the_title();
				break;

			case 'excerpt':
					$output .= get_the_excerpt();
				break;

			case 'body':
					$output .= get_the_content();
					break;

			default:
						$output = 'Please verify requested element';
						//$output .= get_the_content();
			}
		//}

	} else {
		$output .= 'Default consumer faqs not found.';
	}

	wp_reset_postdata();


	return $output;
}
