<?php

//Create shortcodes for custon query vars


add_shortcode('swa_resend_activation_link', 'swa_resend_activation_link_func');

function swa_resend_activation_link_func(){
	global $wp_query;

	//first_name
	if (isset($wp_query->query_vars['first_name']))
	{
		$first_name = $wp_query->query_vars['first_name'];
	} else {
		$first_name = "";
	}

	//email
	if (isset($wp_query->query_vars['email']))
	{
		$email = $wp_query->query_vars['email'];
	} else {
		$email = "";
	}

	//affiliate_id
	if (isset($wp_query->query_vars['affiliate_id']))
	{
		$affiliate_id = $wp_query->query_vars['affiliate_id'];
	} else {
		$affiliate_id = "";
	}

	//activation_url
	if (isset($wp_query->query_vars['activation_url']))
	{
		$activation_url = $wp_query->query_vars['activation_url'];
	} else {
		$activation_url = "";
	}

	//key
	if (isset($wp_query->query_vars['key']))
	{
		$key = $wp_query->query_vars['key'];
	} else {
		$key = "";
	}

	$url = "/replacement-activation-link/?first_name={$first_name}&email={$email}&affilaite_id={$affiliate_id}&activation_url={$activation_url}&key={$key}";
	$link = site_url() . $url;
	$link = "<a href='{$link}'>Please resend my activation link.</a>";
	// PC::debug($link);
	return $link;
}

add_shortcode( 'qvar_first_name', 'swa_qvar_first_name' );

function swa_qvar_first_name(){
	global $wp_query;
	if (isset($wp_query->query_vars['first_name']))
	{
		$shortcode = $wp_query->query_vars['first_name'];
	} else {
		$shortcode = "Query var first_name is not set.";
	}
	return $shortcode;
}

add_shortcode( 'qvar_email', 'swa_qvar_email' );

function swa_qvar_email(){
	global $wp_query;
	if (isset($wp_query->query_vars['email']))
	{
		$shortcode = $wp_query->query_vars['email'];
	} else {
		$shortcode = "Query var email is not set.";
	}
	return $shortcode;
}

add_shortcode( 'qvar_login', 'swa_qvar_login' );

function swa_qvar_login(){
	global $wp_query;
	//login
	if (isset($wp_query->query_vars['login']))
	{
		$login = $wp_query->query_vars['login'];
	} else {
		$login = "";
	}
}

//user_name
add_shortcode( 'qvar_user_login', 'swa_qvar_user_login' );
function swa_qvar_user_login(){
	if (isset($wp_query->query_vars['user_login']))
	{
		$user_name = $wp_query->query_vars['user_login'];
	} else {
		$user_name = "";
	}
}

//user_name
add_shortcode( 'qvar_user_name', 'swa_qvar_user_name' );
function swa_qvar_user_name(){
	if (isset($wp_query->query_vars['user_name']))
	{
		$user_name = $wp_query->query_vars['user_name'];
	} else {
		$user_name = "";
	}
}


add_shortcode( 'qvar_affiliate_id', 'swa_qvar_affiliate_id' );

function swa_qvar_affiliate_id(){
	global $wp_query;
	if (isset($wp_query->query_vars['affiliate_id']))
	{
		$shortcode = $wp_query->query_vars['affiliate_id'];
	} else {
		$shortcode = "Query var affiliate_id is not set.";
	}
	return $shortcode;
}

add_shortcode( 'qvar_activation_url', 'swa_qvar_activation_url' );

function swa_qvar_activation_url(){
	global $wp_query;
	if (isset($wp_query->query_vars['activation_url']))
	{
		$shortcode = $wp_query->query_vars['activation_url'];
	} else {
		$shortcode = "Query var activation_url is not set.";
	}
	return $shortcode;
}


add_shortcode( 'qvar_activation_error', 'swa_qvar_activation_error' );

function swa_qvar_activation_error(){
	global $wp_query;
	if (isset($wp_query->query_vars['activation_error']))
	{
		$shortcode = $wp_query->query_vars['activation_error'];
	} else {
		$shortcode = "Query var activation_error is not set.";
	}
	return $shortcode;
}


add_shortcode( 'qvar_key', 'swa_qvar_key' );

function swa_qvar_key(){
	global $wp_query;
	if (isset($wp_query->query_vars['key']))
	{
		$shortcode = $wp_query->query_vars['key'];
	} else {
		$shortcode = "Query var key is not set.";
	}
	return $shortcode;
}
