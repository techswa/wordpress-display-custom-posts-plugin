<?php

/**
 * Manage custom (non wp) query vars
 *
 * @param
 * @return    void
 * @author
 * @copyright
 */



add_filter('query_vars', 'parameter_queryvars' );

function parameter_queryvars( $qvars )
{
$qvars[] = 'affiliate_id';
$qvars[] = 'first_name';
$qvars[] = 'email';
$qvars[] = 'user_name';
$qvars[] = 'user_login';
$qvars[] = 'login';
$qvars[] = 'activation_url';
$qvars[] = 'key';
$qvars[] = 'nonce';
return $qvars;
}
?>
