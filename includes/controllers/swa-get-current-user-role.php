<?php

/**
 * Returns the current users role
 *
 * @param     object $current_user
 * @return    string $user_role
 * @author
 * @copyright
 */

 function swa_get_current_user_role( $current_user ){
      //  global $current_user;
       wp_get_current_user();
       $user_roles = $current_user->roles;
       $user_role = array_shift($user_roles);


   return $user_role;
 }
