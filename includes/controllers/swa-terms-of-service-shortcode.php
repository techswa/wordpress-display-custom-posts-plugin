<?php

//Create shortcode to embed terms of service text

add_shortcode( 'swa_terms_of_service', 'swa_terms_of_service_func' );

function swa_terms_of_service_func($args){

global $post;
 //$test = $args[page_id];
	$output = '';

	$defaults = array(
		'post_type'	=> 'page',
		'page_id'		=>	'561'
	);
	$args = wp_parse_args( $args, $defaults );

	$swa_query= new WP_Query( $args );
	if ( $swa_query->have_posts() ) {
		//$output .= '<div class="swa-sensei-course-excerpt">';
		while ( $swa_query->have_posts() ) {
			$swa_query->the_post();
      //$output = '<div class="swa-sensei-course-excerpt">';
			$output .= get_the_content();
      //$output .= '</div>';
			//
			// $title = get_the_title();
			// $link = get_the_permalink();
			//
			// $output .= "<li><a href=\"{$link}\">{$title}</a></li>";
		}
    //$output .= '</div>';
	} else {
		$output .= 'Terms of service not found.';
	}

	wp_reset_postdata();


	return $output;
}
