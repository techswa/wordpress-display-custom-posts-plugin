# README #

## This plugin allows multiple custom post types to be displayed via custom shortcodes.

* The the post id is passed through the shortcode and the custom postype is returned.

```php

add_shortcode( 'swa_faq_consumer', 'swa_faq_consumer_func' );

function swa_faq_consumer_func($args){
...
}
```
